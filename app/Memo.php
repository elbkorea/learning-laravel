<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memo extends Model
{
    //
    protected $fillable = ['subject', 'content'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
