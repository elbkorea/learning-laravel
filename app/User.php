<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        // user는 하나의 Profile을 가지고 있고, 그것을 리턴한다
        // 1:1 관계 일 경우에는 메소드명을 단수로 쓰는 것이 관례
        return $this->hasOne(Profile::class);
    }

    public function memos()
    {   
        // user는 여러개의 memo를 가지고 있고, 그 메모 여러개의 collection을 리턴한다
        // 1:N 관계 일 경우에는 메소드명을 복수로 쓰는 것이 관례
        
        return $this->hasMany(Memo::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::calss);
    }

    public function phone()
    {
        return $this->hasOne('App\Phone');
    }
}