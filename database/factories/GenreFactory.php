<?php

use Faker\Generator as Faker;

$factory->define(App\Genre::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ko_KR');
    return [
        'name' => $faker->word,
    ];
});
