<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ko_KR');
    return [
        'title' => $faker->sentence(),
        'content' => $faker->paragraphs(rand(2,10),true)
    ];
});
