<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/blade', function (){
    return view('child');
});


Route::get('/home', function()
{
    return View::make('pages.home');
});
Route::get('about', function()
{
    return View::make('pages.about');
});
Route::get('projects', function()
{
    return View::make('pages.projects');
});
Route::get('contact', function()
{
    return View::make('pages.contact');
});

Route::resource('/film', 'FilmController');
Route::resource('/coupon', 'CouponController');

Route::get('auth/login',function(){
    $loginInfos = [
        'email' => 'rudy01@example.com',
        'password' => 'password'
    ];
    if(!auth()->attempt($loginInfos))
        return 'login 정보가 맞지 않습니다.';
    return redirect('loginOK');
});

// Route::get('loginOK', function () {
//     dump(session()->all());     // 세션 정보 출력

//     if(!auth()->check())        // 로그인을 했으면
//         return '다시 로그인을 시도하세요';
//     return '안녕하세요 '.auth()->user()->name;
// });

Route::get('loginOK', ['middler' => 'auth',function(){
    dump(session()->all());     // 세션 정보 출력
    return '안녕하세요 미들웨어를 이용한 인증 성공 '.auth()->user()->name;
}]);

Route::get('auth/logout', function () {
    auth()->logout();
    return '로그아웃 되었습니다.';
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('impl', 'ImplicitController');
